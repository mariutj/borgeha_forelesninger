import numpy as np

x = np.linspace(-3.14, 3.14, 21)
y = np.cos(x)

print("x:", x)
print("y:", y)