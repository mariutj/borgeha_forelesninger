import numpy as np
import random

a = np.array([[random.randint(1,10) for i in range(20)]])
b = np.reshape(a,(4,5))
c = np.reshape(a,(2,10))
print(f'\n{a}\n')
print(b,'\n')
print(c)
print(c.dtype)