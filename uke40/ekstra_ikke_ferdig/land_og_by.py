# Denne koden er ikke ferdig! Det jobbes med ukens kode!

# Helt først: Bare ett land og en by

land = 'Norge'
by = 'Oschlo'

gjettet = input(f'Hovedstanden i {land}? ')

if gjettet == by:
    print("UDAMAN")
else:
    print('tihihihih!')



# Men hva om vi ønsker å gjøre om dette så det støtter flere land og
# hovedsteder? Da er lister en perfekt ting!

alle_land = ['Norge', 'Sverige', 'Danmark', 'Tyskland']
alle_hovedsteder = ['Oslo', 'Stockholm', 'København', 'Berlin']

# Vi kan trekke ut et tilfeldig tall fra 0 - lengden av alle_land -1:
import random
tilfeldig_tall = random.randint(0, len(alle_land) -1) # hvorfor minus 1?

gjettet = input(f'Hovedstanden i {alle_land[tilfeldig_tall]}? ')

if gjettet == alle_hovedsteder[tilfeldig_tall]:
    print("UDAMAN")
else:
    print('tihihihih!')




# Vi kan gå igjennom alle 
import random
tilfeldig_tall = random.randint(0, len(alle_land) -1) # hvorfor minus 1?

gjettet = input(f'Hovedstaden i {alle_land[tilfeldig_tall]}? ')

if gjettet == alle_hovedsteder[tilfeldig_tall]:
    print("UDAMAN")
else:
    print('tihihihih!')


# La oss leke oss litt med alle_land.

# Kan vi sortere landene?
# hvorfor er dette feil?: alle_land = alle_land.sort()
# Vi må heller bare skrive alle_land.sort()

# Vi kan sjekke om et land er med i listen:
print(f'Norge er i alle_land: {"Norge" in alle_land}')
print(f'Norge er i alle_land: {"Sveits" in alle_land}')

# Husker dere at en kunne kose seg med :: i range og slikt? Gjett hva.
# man kan gjøre sånt med lister også! Det kalles slicing
print(f'De første to landene er {alle_land[:2]}')
print(f'De første to landene Land i motsatt rekkefølge: {alle_land[::-1]}')
