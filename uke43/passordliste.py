'''
Dette er en veldig enkel greie å vise hvordan en kan
bruke todimensjonale lister for å holde rede på kobling
mellom to dataverdier. 
'''

# Leter igjennom passordliste, etter treff
def finn_passord(passordliste):
    nettsted = input('Nettsted du vil ha passord til: ')
    for sted in passordliste:
        if sted[0] == nettsted:
            return sted[1] # Dette er jo passordet
    return None # Default


liste = [['reddit', '''¯\_(ツ)_/¯'''], ['youtube', 'passordYT!'], \
         ['ute.no', 'O$teSkrakkeleringsGnøffe_Gnøff'], \
         ['filateli.no', 'Frimerker4ever']]

# print('finn_passord: ' + finn_passord(liste))