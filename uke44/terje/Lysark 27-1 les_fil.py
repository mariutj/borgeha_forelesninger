filnavn = input('Oppgi navn på fila: ')
try:
    f = open(filnavn,'r')    # Åpner fila for lesing
    innhold = f.read()       # Leser inn hele driten på en gang
    f.close()
    print(innhold)
except IOError:
    print('Finner ikke fila.')